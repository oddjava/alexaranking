package alexa;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import com.mongodb.BasicDBObject;
import com.mongodb.Bytes;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class Alexa 
{
	// References related to Mongo DB.
	MongoClient mongoClient;
	DB company;
	DBCollection parsedDataForVPS;

	// Constructor to establish DB connection.
	Alexa(String ip)
	{
		mongoClient = new MongoClient(ip, 27017);
		company = mongoClient.getDB("company");
		parsedDataForVPS=company.getCollection("parsedDataForVPS");	//ForVPS	
	}
	
	// Main method
	public static void main(String[] args) 
	{		
		new Alexa(args[0]).getAlexa(Integer.parseInt(args[1]),Integer.parseInt(args[2]));
		System.exit(0);	// return status 0 to shell script indicating normal termination.
	}
	
	// Get documents from DB and call get alexa for link
	void getAlexa(int startid,int endid)
	{
		DBCursor cursor = parsedDataForVPS.find(new BasicDBObject("_id", new BasicDBObject("$gte", startid).append("$lte", endid)).append("sellerStatus" , "validDomain").append("alexaStatus", "false"));
		cursor.addOption(Bytes.QUERYOPTION_NOTIMEOUT);
		
		int cursorCount=cursor.count();
		System.out.println("\nTotal="+cursorCount+"\n");
		
		// Start Timer
		long startTime = System.currentTimeMillis();
		
		DBObject present=null;
		String link;
		String id;		
		
		// Get all previous code Status
		String sellerStatus;
		String metaDescriptionStatus;
		String contactInfoStatus;
		String imageStatus;
		String sitetechStatus;
		
		double loads[];
		while (cursor.hasNext()) 
		{
			loads=getLoadAvg();
			if(loads[0]<1.50) // Check load Average < 1.5 
			{
				// Initialize to empty string to avoid previous iteration results.
				link="";
				id="";
				sellerStatus="";
				metaDescriptionStatus="";
				contactInfoStatus="";
				imageStatus="";
				sitetechStatus="";

				present = cursor.next();
				link = present.get("link").toString();
				id=present.get("_id").toString();

				sellerStatus=present.get("sellerStatus").toString();
				metaDescriptionStatus=present.get("metaDescriptionStatus").toString();
				contactInfoStatus=present.get("contactInfoStatus").toString();
				imageStatus=present.get("imageStatus").toString();
				sitetechStatus=present.get("sitetechStatus").toString();

				// If all status are true then set final status as true and process document for alexa.
				if(sellerStatus.equals("validDomain") && metaDescriptionStatus.equals("true") &&
						contactInfoStatus.equals("true") && imageStatus.equals("true") && sitetechStatus.equals("true"))
				{
					new MyRunnable(id,link,parsedDataForVPS).run();
				}
				else
				{
					System.out.println(id+" Rejected. Not all required status true.\n");
				}	
			}
			else // When cpu load average exceed 1.5 code will get terminate.
			{
				System.out.println("So tired. I'm Shuting Nowwwwwww........"+(new SimpleDateFormat("yyyy-MM-dd HH:mm a").format(new Date())));
				System.exit(123);	// return status 123 to shell script indicating abnormal termination.			
			}	
		}
				
		long endTime = System.currentTimeMillis();
		long minutes = TimeUnit.MILLISECONDS.toMinutes(endTime - startTime);
		System.out.println("Total Count="+cursorCount);
		System.out.println("It tooks " + minutes + " min");
	}
	
	// This method will get current load average of CPU using "uptime" command.
		double[] getLoadAvg()
		{
			StringBuffer output = new StringBuffer();
			double loads[]=new double[3];
	        Process p;
	        try {
	                p = Runtime.getRuntime().exec("uptime");	// execute "uptime" command on linux only.
	                p.waitFor();
	                BufferedReader reader =
	                    new BufferedReader(new InputStreamReader(p.getInputStream()));

	                String line = "";
	                while ((line = reader.readLine())!= null) {
	                     output.append(line + "\n"); //Output of command
	                }

	        } catch (Exception e) 
	        {
	                e.printStackTrace();
	        }
	        String str=output.toString();
	        str=str.split("load average[s:][: ]")[1];  //To get only load avg from command output
	        System.out.println("Load Average= "+str);
	        String allValues[]=str.split(",");
	        loads[0]=Double.parseDouble(allValues[0].trim()); //last one Min
	        loads[1]=Double.parseDouble(allValues[1].trim()); //last five Min
	        loads[2]=Double.parseDouble(allValues[2].trim()); //last fifteen Min
	       // System.out.println(lastOneMin+"**"+lastFiveMin+"**"+lastFifteenMin+"**");
	        return loads;
		}
}	
	
class MyRunnable
{
	String id;
	String link;
	DBCollection parsedDataForVPS;
		
	MyRunnable(String id,String link,DBCollection parsedDataForVPS)
	{
		this.id=id;
		this.link=link;
		this.parsedDataForVPS=parsedDataForVPS;
	}
	
	void run() // Get alexa using data.alexa.com API.
	{
		long result = 0;

		String url = "http://data.alexa.com/data?cli=10&url=" + link;
		try
		{
			//Connect to url
			URLConnection conn = new URL(url).openConnection();
			InputStream is = conn.getInputStream();

			//This url returns xml output
			DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();
			Document doc = dBuilder.parse(is);
			
			/* XML sample Output
			 * <ALEXA VER="0.9" URL="google.com/" HOME="0" AID="=" IDN="google.com/">
				<SD>
					<POPULARITY URL="google.com/" TEXT="1" SOURCE="panel"/>
						<REACH RANK="1"/>
						<RANK DELTA="+0"/>
						<COUNTRY CODE="US" NAME="United States" RANK="1"/>
				</SD>
				</ALEXA>
			 * */
			
			//XML parsing
			Element element = doc.getDocumentElement();

			NodeList nodeList = element.getElementsByTagName("POPULARITY");
			if (nodeList.getLength() > 0) 
			{
				Element elementAttribute = (Element) nodeList.item(0);
				String ranking = elementAttribute.getAttribute("TEXT");
				if(!"".equals(ranking))
				{
					result = Long.valueOf(ranking);  //Get alexa Rank
				}
			}
			
			System.out.println(id+"  "+result+"  "+link+"\t"+(new SimpleDateFormat("yyyy-MM-dd HH:mm a").format(new Date()))+"\n");	
			
			//update alexa rank and alexastatus in DB.
			parsedDataForVPS.update(new BasicDBObject("link",link),new BasicDBObject("$set",new BasicDBObject("alexaRanking",result).append("alexaStatus" , "true").append("status" , "true")));
		} 
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
}